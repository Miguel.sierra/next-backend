const Post = require('../../models/post');
const myCache = require('../../services/cache');

const deletePost = async (req, res) => {
  try {
    let id = req.params.id;
    await Post.delete({ _id: id });
    if (myCache.has('post')) {
      myCache.del('post');
    }
    res.status(200).send({ success: true, message: 'deleted Post' });
  } catch (error) {
    if (myCache.has('post')) {
      myCache.del('post');
    }
    res.status(400).send({
      success: false,
      error: error,
      message: 'Error on delete Post'
    });
  }
};

module.exports = deletePost;
