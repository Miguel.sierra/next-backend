const Post = require('../../models/post');
const { isEmptyObject } = require('../../utils/objects');
const myCache = require('../../services/cache');

const getPosts = async (req, res) => {
  try {
    let posts = await Post.find();
    if (isEmptyObject(posts)) {
      res
        .status(404)
        .send({ success: false, posts: {}, message: 'No records' });
    } else {
      myCache.set('post', posts, 100000);
      res.json({ success: true, posts, message: 'Posts retrieved' });
    }
  } catch (error) {
    res.status(400).send({
      success: false,
      error: error._message,
      message: 'Error on get Post'
    });
  }
};

module.exports = getPosts;
