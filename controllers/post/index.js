const create = require('./createPost');
const getPosts = require('./getPosts');
const deletePost = require('./deletePost');
const updatePost = require('./updatePost');

module.exports = {
  create,
  get: getPosts,
  destroy: deletePost,
  update: updatePost
};
