const postValidator = require('../../validators/post');
const { isEmptyObject } = require('../../utils/objects');
const myCache = require('../../services/cache');
const Post = require('../../models/post');

const updatePost = async (req, res) => {
  try {
    let { id } = req.params;
    var post = new Post(req.body);
    delete post._doc._id;
    const validations = await postValidator(post);
    if (!isEmptyObject(validations)) {
      res.status(400).send({ success: false, message: validations });
    } else {
      if (myCache.has('post')) {
        myCache.del('post');
      }
      await Post.findOneAndUpdate({ _id: id }, post, {
        new: false,
        useFindAndModify: false
      });
      res.status(201).send({ success: true, message: 'Post updated' });
    }
  } catch (error) {
    console.log(error);
    if (myCache.has('post')) {
      myCache.del('post');
    }
    res.status(400).send({
      success: false,
      error: error._message,
      message: 'Error on update Post'
    });
  }
};

module.exports = updatePost;
