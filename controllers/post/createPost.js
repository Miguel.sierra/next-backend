const postValidator = require('../../validators/post');
const { isEmptyObject } = require('../../utils/objects');
const myCache = require('../../services/cache');
const Post = require('../../models/post');

const createPost = async (req, res) => {
  try {
    var post = new Post(req.body);
    const validations = await postValidator(post);
    if (!isEmptyObject(validations)) {
      res.status(400).send({ success: false, message: validations });
    } else {
      await post.save();
      let cachePosts = [];
      if (myCache.has('post')) {
        cachePosts = myCache.get('post');
        cachePosts.push(
          post.toObject({
            getters: true,
            transform: (doc, ret) => {
              delete ret._id;
              return ret;
            }
          })
        );
        myCache.set('post', cachePosts, 10000);
      }
      res.status(201).send({ success: true, post, message: 'Post created' });
    }
  } catch (error) {
    res.status(400).send({
      success: false,
      error: error._message,
      message: 'Error on create Post'
    });
  }
};

module.exports = createPost;
