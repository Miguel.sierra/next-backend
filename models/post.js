const mongoose = require('mongoose');
const { Schema } = mongoose;
const mongoose_delete = require('mongoose-delete');

const PostsSchema = new Schema({
  post: {
    type: String,
    required: false
  },
  status: {
    type: String,
    required: false,
    default: 'incomplete'
  },
  createdAt: {
    type: Date,
    required: false
  }
});

PostsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

PostsSchema.pre('save', async function(next) {
  // Hash the password before saving the user model
  const post = this;
  post.createdAt = new Date();
  next();
});

const Post = mongoose.model('Post', PostsSchema);

module.exports = Post;
