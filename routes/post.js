//Server import
const express = require('express');
const router = express.Router();
const { create, get, update, destroy } = require('../controllers/post');
const cacheMiddleware = require('../middlewares/checkCache');

/**
 * @typedef Post
 * @property {string} post - Description - eg: Test description
 */

/**
 * Add Post
 * @route POST /post
 * @group Post
 * @param {Post.model} body.body
 * @returns {object} 200 - Success
 * @returns {Post.model} 400 - Error
 */
router.post('/post', create);

/**
 * get Post
 * @route GET /posts
 * @group Post
 * @returns {object} 200 - Success
 * @returns {Post.model} 400 - Error
 */
router.get('/posts', cacheMiddleware, get);

/**
 * This function comment is parsed by doctrine
 * @route DELETE /post/{id}
 * @group Post
 * @param {string} id.path.required - id - eg: 124124213213
 * @returns {object} 200 - Success
 * @returns {Error}  default - Unexpected error
 */
router.delete('/post/:id', destroy);

/**
 * update Post
 * @route PUT /post/{id}
 * @group Post
 * @param {string} id.path.required - id - eg: 124124213213
 * @param {Post.model} body.body
 * @returns {object} 200 - Success
 * @returns {Post.model} 400 - Error
 */
router.put('/post/:id', update);

module.exports = router;
