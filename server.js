'use strict';
//Server imports
const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const mongoose = require('mongoose');
const morgan = require('morgan');
require('dotenv').config();

//Server env vars
const port = process.env.PORT || 3000;

//Server routes
const post = require('./routes/post');

//Database
// Mongoose with promises
mongoose.Promise = global.Promise;
//Initial connection database

let mongoConnection = {
  MONGO_HOST: process.env.MONGO_HOST,
  MONGO_PASSWORD: process.env.MONGO_PASSWORD
};

mongoose
  .connect(
    `mongodb+srv://${mongoConnection.MONGO_HOST}:${mongoConnection.MONGO_PASSWORD}@cluster0-q3pyw.mongodb.net/test?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }
  )
  .then(() => {
    // Cuando se realiza la conexión, lanzamos este mensaje por consola
    //Server initial configuration
    let app = express();
    if (process.env.NODE_ENV === 'development') {
      const expressSwagger = require('express-swagger-generator')(app);
      let options = {
        swaggerDefinition: {
          info: {
            description: 'This is a sample server',
            title: 'Swagger',
            version: '1.0.0'
          },
          host: 'localhost:3001',
          basePath: '/api',
          produces: ['application/json', 'application/xml'],
          schemes: ['http']
        },
        basedir: __dirname, //app absolute path
        files: ['./routes/*.js'] //Path to the API handle folder
      };
      expressSwagger(options);
    }

    app.use(cors());
    app.use(morgan('dev'));
    app.use(helmet());
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({ extended: true }));

    //Server Routes
    app.use('/api/', post);

    app.get('/', (req, res) => {
      res.send('ok');
    });

    //Initialize server
    app.listen(port, () => {
      console.log(`Our server is running on port ${port}`);
    });
  })
  // Si no se conecta correctamente escupimos el error
  .catch(err => console.log(err));
