module.exports = async function(post) {
  let errors = [];
  try {
    if (!post.status && !post.post) {
      errors.push('status or post required');
    }
  } catch (error) {
    errors.push(error);
  }

  return errors;
};
