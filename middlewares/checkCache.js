const myCache = require('../services/cache');

const checkCache = async (req, res, next) => {
  try {
    if (myCache.has('post')) {
      let posts = myCache.get('post');
      res
        .status(200)
        .json({ success: true, posts, message: 'Posts retrieved' });
    } else {
      next();
    }
  } catch (error) {
    if (myCache.has('post')) {
      myCache.del('post');
    }
    res.status(500).send(error);
  }
};

module.exports = checkCache;
