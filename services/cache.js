const NodeCache = require("node-cache");
const myCache = new NodeCache({ stdTTLL: 0, checkperiod: 86400 });

module.exports = myCache;
